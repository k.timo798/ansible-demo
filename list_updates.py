#!/usr/bin/env python
# coding=utf-8

import apt
import apt_pkg
from time import strftime
import os
import subprocess
import sys
import json
import socket 

SYNAPTIC_PINFILE = "/var/lib/synaptic/preferences"
DISTRO = subprocess.check_output(["lsb_release", "-c", "-s"], universal_newlines=True).strip()


def clean(cache, depcache):
    depcache.init()


def saveDistUpgrade(cache, depcache):
    depcache.upgrade(True)
    if depcache.del_count > 0:
        clean(cache, depcache)
    depcache.upgrade()


def get_update_packages():
    pkgs = []

    apt_pkg.init()
    apt_pkg.config.set("Dir::Cache::pkgcache", "")

    try:
        cache = apt_pkg.Cache(apt.progress.base.OpProgress())
    except SystemError as e:
        sys.stderr.write("Error: Opening the cache (%s)" % e)
        sys.exit(-1)

    depcache = apt_pkg.DepCache(cache)
    depcache.read_pinfile()
    if os.path.exists(SYNAPTIC_PINFILE):
        depcache.read_pinfile(SYNAPTIC_PINFILE)
    depcache.init()

    try:
        saveDistUpgrade(cache, depcache)
    except SystemError as e:
        sys.stderr.write("Error: Marking the upgrade (%s)" % e)
        sys.exit(-1)

    pkgs = []
    for pkg in cache.packages:
        if not (depcache.marked_install(pkg) or depcache.marked_upgrade(pkg)):
            continue
        inst_ver = pkg.current_ver
        cand_ver = depcache.get_candidate_ver(pkg)
        if cand_ver == inst_ver:
            continue
        record = {
            "name": pkg.name,
            "security": isSecurityUpgrade(pkg, depcache),
            "section": pkg.section,
            "current_version": inst_ver.ver_str if inst_ver else '-',
            "candidate_version": cand_ver.ver_str if cand_ver else '-',
            "priority": cand_ver.priority_str
        }
        pkgs.append(record)

    return pkgs


def isSecurityUpgrade(pkg, depcache):
    def isSecurityUpgrade_helper(ver):
        security_pockets = [("Ubuntu", "%s-security" % DISTRO),
                            ("gNewSense", "%s-security" % DISTRO),
                            ("Debian", "%s-updates" % DISTRO)]

        for (file, index) in ver.file_list:
            for origin, archive in security_pockets:
                if (file.archive == archive and file.origin == origin):
                    return True
        return False

    inst_ver = pkg.current_ver
    cand_ver = depcache.get_candidate_ver(pkg)

    if isSecurityUpgrade_helper(cand_ver):
        return True

    for ver in pkg.version_list:
        if (inst_ver and
            apt_pkg.version_compare(ver.ver_str, inst_ver.ver_str) <= 0):
            continue
        if isSecurityUpgrade_helper(ver):
            return True

    return False

if __name__ == '__main__':
    pkgs = get_update_packages()
    
    # Zeitstempel für die Ausführung des Skripts
    timestamp = strftime('%m/%d/%Y %H:%M:%S')
    hostname = socket.getfqdn()

    for pkg in pkgs:
        update_json = {
            "hostname": hostname,
            "update_checked_time": timestamp,
            "packages_name": pkg["name"],
            "packages_section": pkg["section"],
            "packages_priority": pkg["priority"],
            "packages_current_version": pkg["current_version"],
            "packages_security": str(pkg["security"]).lower(),
            "packages_candidate_version": pkg["candidate_version"]
        }
        json_output = json.dumps(update_json, indent=None, separators=(',', ':'))
        print(json_output)
